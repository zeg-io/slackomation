const { RTMClient } = require('@slack/rtm-api')
const { WebClient } = require('@slack/web-api')
const { prompt } = require('enquirer')
const dotenv = require('dotenv')
const chalk = require('chalk')
const Configstore = require('configstore')
const packageJson = require('../package.json')
const config = new Configstore(packageJson.name, { email: null })
const { slackBotTokenKey, emailKey } = require('./constants')

const {
  watched,
  setUser,
  registeredUserId,
  subscribeActions,
  setRegisteredUser
} = require('../lib/watched-user')
const messageHandler = require('../lib/events/message')
const presenceChangeHandler = require('../lib/events/presence-change')
const userChangeHandler = require('../lib/events/user-change')
const boolPrompt = require('../lib/prompt/bool')
const monitorUsb = require('../lib/monitor-usb')

dotenv.config()

let retryTimeout = 3
let retries = 0

let luxafor // action for animation
let off // action lights off

let rtm // Slack RealTimeMessages client
let wc // Slack WebClient

// Luxafor USB FLAG vendor and product ID
const vendorId = 1240
const productId = 62322

monitorUsb.init({
  deviceName: 'Luxafor Flag Device',
  vendorId,
  productId,
  onConnect: async () => {
    status.luxaforDetected = true
    console.info('connected')
    if (status.slackBotIdValid) {
      // const user = (await wc.users.info({ user: watched().id })).user
      setTimeout(subscribeActions, 3000)
    } else {
      return await main()
    }
  },
  onDisconnect: () => {
    status.luxaforDetected = false
    console.error('disconnected')
    return
  }
})

const status = {
  luxaforDetected: false,
  slackBotIdValid: false,
  slackBotInitialized: false,
  watchedEmailValid: false
}

const getUserByEmail = async email => {
  try {
    const user = await wc.users.lookupByEmail({ email })
    return user.user
  } catch (e) {
    if (e.message.includes('not_authed')) {
      console.error('Slack Bot Token is invalid.')
      config.set(slackBotTokenKey)
      main()
    } else {
      throw e
    }
  }
}

const validateConfiguration = async () => {
  const email = config.get(emailKey)
  const slackBotToken = config.get(slackBotTokenKey)
  let prompts = []

  if (!slackBotToken) {
    prompts.push({
      type: 'input',
      name: slackBotTokenKey,
      message: 'What is your slack bot token?  (must be a legacy bot)'
    })
  }
  if (!email || email.length < 5) {
    prompts.push({
      type: 'input',
      name: emailKey,
      message: 'Email address for the status you wish to watch?'
    })
  } else {
    console.info(chalk.green(`Slack tracking user: [${chalk.cyan(email)}]`))
  }
  if (prompts.length > 0) {
    luxafor()
    const response = await prompt(prompts)

    Object.keys(response).forEach(key => {
      config.set(key, response[key])
    })
    off()
  }
}

const tryAgainIn = seconds => {
  setTimeout(() => {
    retries += 1
    process.stdout.write(`...Retry ${retries}: `)
    retryTimeout = retryTimeout * 3
    if (retryTimeout > 60 * 60) {
      return console.error('ERROR: Timeout Exceeded')
    }
    main()
  }, seconds * 1000)
}

const main = async () => {
  if (!status.luxaforDetected) {
    try {
      // Check for a light
      if (!monitorUsb.isConnected()) {
        console.error(`Luxafor Flag Device Not Connected [~131].`)
        return
      }

      const actions = require('../lib/actions')
      actions.init({ vendorId, productId })
      off = actions.off
      luxafor = actions.luxafor
      // Lights off
      off()
      status.luxaforDetected = true
    } catch (e) {
      if (e.message.includes('cannot open device')) {
        console.warn('No Luxafor light detected!')
        // console.warn(`...Trying again in ${retryTimeout} seconds.`)
        return
      } else {
        throw e
      }
      return
    }
  } else {
    // success, so ensure defaults are reset.
    retryTimeout = 3
    retries = 0
  }
  if (!status.slackBotIdValid && status.luxaforDetected) {
    try {
      await validateConfiguration()

      // Read a token from the environment variables
      const token = await config.get(slackBotTokenKey)

      // Initialize
      rtm = new RTMClient(token)
      wc = new WebClient(token)

      const testResult = await wc.auth.test({
        token: config.get(slackBotTokenKey)
      })
      if (testResult.ok) {
        status.slackBotIdValid = true
      }
    } catch (e) {
      if (e.message.includes('invalid_auth')) {
        const errMsg = `
ERROR: Invalid Slack Bot Token
       Current Token [${chalk.yellow(config.get(slackBotTokenKey))}]
`
        console.error(errMsg)

        if (
          await boolPrompt('reset', 'Do you wish to reset the Slack Bot Token?')
        ) {
          await config.set(slackBotTokenKey, null)
          await main()
        }
      }
    }
  }

  if (!status.watchedEmailValid && status.slackBotIdValid) {
    const watchedEmail = config.get(emailKey)
    // VALIDATE SLACK CONNECTION
    try {
      const user = await getUserByEmail(watchedEmail)
      setRegisteredUser(user)
      setUser(user)

      // let watched = getUser()

      status.watchedEmailValid = true
    } catch (e) {
      if (e.message.includes('users_not_found')) {
        console.error(
          `ERROR: Watched email [${chalk.yellow(watchedEmail)}] was not found.`
        )

        if (
          await boolPrompt('reset', 'Do you wish to reset the email address?')
        ) {
          await config.set(emailKey, '')
          await main()
        }
      } else {
        throw e
      }
    }
    console.info('end')
  }
  if (status.slackBotIdValid && !status.slackBotInitialized) {
    // Connect to Slack
    const { self, team } = await rtm.start()

    rtm.subscribePresence([watched().id])
    console.info(`Subscribed to user [${watched().name}]`)

    rtm.on('message', messageHandler(wc))
    rtm.on('presence_change', presenceChangeHandler(wc))
    rtm.on('user_change', userChangeHandler)
    status.slackBotIdValid = true
    status.slackBotInitialized = true
  }
}
main()
