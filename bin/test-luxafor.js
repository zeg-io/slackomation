const colorMapper = require('../lib/luxafor/color-map')
const Luxafor = require('../lib/luxafor')
const lux = new Luxafor()

// lux.color(red, green, blue)
// lux.color(255, 0, 0).exec()

/*
 * Luxafor LEDs are
 *
 * FRONT:   BACK:
 * 3        6
 * 2        5
 * 1        4
 *
 * and BACK:
 *
 * */
// Commands
// {
//   "color": 1,
//   "fade": 2,
//   "strobe": 3,
//   "wave": 4,
//   "pattern": 6
// }
const countdown = require('../lib/actions/lux/countdown')

const flash = require('../lib/actions/lux/flash')

const colorName = require('../lib/luxafor/color-name')

const main = async () => {
  lux.color(0, 0, 0).exec()

  await flash(lux, 5, {
    lightArray: [4, 5, 6],
    colorArray: ['red', 'yellow', 'green'],
    speed: 100
  }).then(() => {
    console.info('done')
  })

  // setTimeout(() => lux.fade(0, 255, 0, 100).exec(), 3000)

  // countDown(1, {
  //   lightArray: [4, 5, 6],
  //   colorArray: ['green', 'yellow', 'red']
  // }).then(() => {
  // lux.pattern('police').exec()

  // setTimeout(() => {
  //   lux.colorName('red').exec()
  // }, 2000)
  // })

  // lux.fade(255, 0, 0, 100).exec()
  //
  // setTimeout(() => lux.fade(0, 255, 0, 100).exec(), 3000)
  // setTimeout(() => lux.fade(0, 255, 0, 100).exec(), 3000)
}

main()
