const slackConversation = require('../actions/slack-conversation')
const { watched, registeredUserId } = require('../watched-user')

module.exports = wc => event => {
  const registeredId = registeredUserId()
  console.info('MESSAGE:', event)

  if (event && registeredId === event.user) {
    console.info('MESSAGE:', event.text)
    console.info('RESPOND:', slackConversation(event, wc))
  }
}
