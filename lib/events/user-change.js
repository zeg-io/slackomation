const { watched, setUser, registeredUserId } = require('../watched-user')

module.exports = data => {
  const registeredId = registeredUserId()

  if (!registeredId || registeredId === data.user.id) {
    setUser(data.user)
    console.info('⚡ User change:', watched().status)
  }
}
