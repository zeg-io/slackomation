const { watched, setUser } = require('../watched-user')

const refreshUser = async (id, wc) => (await wc.users.info({ user: id })).user
module.exports = wc => async data => {
  console.info('⚡ Presence changed:', data.presence)

  const user = await refreshUser(watched().id, wc)
  setUser(user)
  console.info('  Refreshed user:', watched().status)
}
