const moment = require('moment')

const userProfileSimplifier = user => {
  const statusText = user.profile.status_text
  const statusEmoji = user.profile.status_emoji
  const statusExpiration = user.profile.status_expiration

  return {
    id: user.id,
    teamId: user.team_id,
    name: user.real_name,
    username: user.name,
    status: {
      text: statusText || '',
      emoji: statusEmoji || '',
      exp: statusExpiration || 0,
      expText:
        statusExpiration && statusExpiration !== 0
          ? moment.unix(statusExpiration).calendar()
          : 0
    }
  }
}

module.exports = userProfileSimplifier
