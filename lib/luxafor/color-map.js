module.exports = {
  black: {
    r: 0x00,
    g: 0x00,
    b: 0x00
  },
  night: {
    r: 0x0c,
    g: 0x09,
    b: 0x0a
  },
  gunmetal: {
    r: 0x2c,
    g: 0x35,
    b: 0x39
  },
  midnight: {
    r: 0x2b,
    g: 0x1b,
    b: 0x17
  },
  charcoal: {
    r: 0x34,
    g: 0x28,
    b: 0x2c
  },
  'dark slate grey': {
    r: 0x25,
    g: 0x38,
    b: 0x3c
  },
  oil: {
    r: 0x3b,
    g: 0x31,
    b: 0x31
  },
  'black cat': {
    r: 0x41,
    g: 0x38,
    b: 0x39
  },
  iridium: {
    r: 0x3d,
    g: 0x3c,
    b: 0x3a
  },
  'black eel': {
    r: 0x46,
    g: 0x3e,
    b: 0x3f
  },
  'black cow': {
    r: 0x4c,
    g: 0x46,
    b: 0x46
  },
  'gray wolf': {
    r: 0x50,
    g: 0x4a,
    b: 0x4b
  },
  'vampire gray': {
    r: 0x56,
    g: 0x50,
    b: 0x51
  },
  'gray dolphin': {
    r: 0x5c,
    g: 0x58,
    b: 0x58
  },
  'carbon gray': {
    r: 0x62,
    g: 0x5d,
    b: 0x5d
  },
  'ash gray': {
    r: 0x66,
    g: 0x63,
    b: 0x62
  },
  'cloudy gray': {
    r: 0x6d,
    g: 0x69,
    b: 0x68
  },
  'smokey gray': {
    r: 0x72,
    g: 0x6e,
    b: 0x6d
  },
  gray: {
    r: 0x73,
    g: 0x6f,
    b: 0x6e
  },
  granite: {
    r: 0x83,
    g: 0x7e,
    b: 0x7c
  },
  'battleship gray': {
    r: 0x84,
    g: 0x84,
    b: 0x82
  },
  'gray cloud': {
    r: 0xb6,
    g: 0xb6,
    b: 0xb4
  },
  'gray goose': {
    r: 0xd1,
    g: 0xd0,
    b: 0xce
  },
  platinum: {
    r: 0xe5,
    g: 0xe4,
    b: 0xe2
  },
  'metallic silver': {
    r: 0xbc,
    g: 0xc6,
    b: 0xcc
  },
  'blue gray': {
    r: 0x98,
    g: 0xaf,
    b: 0xc7
  },
  'light slate gray': {
    r: 0x6d,
    g: 0x7b,
    b: 0x8d
  },
  'slate gray': {
    r: 0x65,
    g: 0x73,
    b: 0x83
  },
  'jet gray': {
    r: 0x61,
    g: 0x6d,
    b: 0x7e
  },
  'mist blue': {
    r: 0x64,
    g: 0x6d,
    b: 0x7e
  },
  'marble blue': {
    r: 0x56,
    g: 0x6d,
    b: 0x7e
  },
  'slate blue': {
    r: 0x73,
    g: 0x7c,
    b: 0xa1
  },
  'steel blue': {
    r: 0x48,
    g: 0x63,
    b: 0xa0
  },
  'blue jay': {
    r: 0x2b,
    g: 0x54,
    b: 0x7e
  },
  'dark slate blue': {
    r: 0x2b,
    g: 0x38,
    b: 0x56
  },
  'midnight blue': {
    r: 0x15,
    g: 0x1b,
    b: 0x54
  },
  'navy blue': {
    r: 0x00,
    g: 0x00,
    b: 0x80
  },
  'blue whale': {
    r: 0x34,
    g: 0x2d,
    b: 0x7e
  },
  'lapis blue': {
    r: 0x15,
    g: 0x31,
    b: 0x7e
  },
  'denim dark blue': {
    r: 0x15,
    g: 0x1b,
    b: 0x8d
  },
  'earth blue': {
    r: 0x00,
    g: 0x00,
    b: 0xa0
  },
  'cobalt blue': {
    r: 0x00,
    g: 0x20,
    b: 0xc2
  },
  'blueberry blue': {
    r: 0x00,
    g: 0x41,
    b: 0xc2
  },
  'sapphire blue': {
    r: 0x25,
    g: 0x54,
    b: 0xc7
  },
  'blue eyes': {
    r: 0x15,
    g: 0x69,
    b: 0xc7
  },
  'royal blue': {
    r: 0x2b,
    g: 0x60,
    b: 0xde
  },
  'blue orchid': {
    r: 0x1f,
    g: 0x45,
    b: 0xfc
  },
  'blue lotus': {
    r: 0x69,
    g: 0x60,
    b: 0xec
  },
  'light slate blue': {
    r: 0x73,
    g: 0x6a,
    b: 0xff
  },
  'windows blue': {
    r: 0x35,
    g: 0x7e,
    b: 0xc7
  },
  'glacial blue ice': {
    r: 0x36,
    g: 0x8b,
    b: 0xc1
  },
  'silk blue': {
    r: 0x48,
    g: 0x8a,
    b: 0xc7
  },
  'blue ivy': {
    r: 0x30,
    g: 0x90,
    b: 0xc7
  },
  'blue koi': {
    r: 0x65,
    g: 0x9e,
    b: 0xc7
  },
  'columbia blue': {
    r: 0x87,
    g: 0xaf,
    b: 0xc7
  },
  'baby blue': {
    r: 0x95,
    g: 0xb9,
    b: 0xc7
  },
  'light steel blue': {
    r: 0x72,
    g: 0x8f,
    b: 0xce
  },
  'ocean blue': {
    r: 0x2b,
    g: 0x65,
    b: 0xec
  },
  'blue ribbon': {
    r: 0x30,
    g: 0x6e,
    b: 0xff
  },
  'blue dress': {
    r: 0x15,
    g: 0x7d,
    b: 0xec
  },
  'dodger blue': {
    r: 0x15,
    g: 0x89,
    b: 0xff
  },
  'cornflower blue': {
    r: 0x64,
    g: 0x95,
    b: 0xed
  },
  'sky blue': {
    r: 0x66,
    g: 0x98,
    b: 0xff
  },
  'butterfly blue': {
    r: 0x38,
    g: 0xac,
    b: 0xec
  },
  iceberg: {
    r: 0x56,
    g: 0xa5,
    b: 0xec
  },
  'crystal blue': {
    r: 0x5c,
    g: 0xb3,
    b: 0xff
  },
  'deep sky blue': {
    r: 0x3b,
    g: 0xb9,
    b: 0xff
  },
  'denim blue': {
    r: 0x79,
    g: 0xba,
    b: 0xec
  },
  'light sky blue': {
    r: 0x82,
    g: 0xca,
    b: 0xfa
  },
  'day sky blue': {
    r: 0x82,
    g: 0xca,
    b: 0xff
  },
  'jeans blue': {
    r: 0xa0,
    g: 0xcf,
    b: 0xec
  },
  'blue angel': {
    r: 0xb7,
    g: 0xce,
    b: 0xec
  },
  'pastel blue': {
    r: 0xb4,
    g: 0xcf,
    b: 0xec
  },
  'sea blue': {
    r: 0xc2,
    g: 0xdf,
    b: 0xff
  },
  'powder blue': {
    r: 0xc6,
    g: 0xde,
    b: 0xff
  },
  'coral blue': {
    r: 0xaf,
    g: 0xdc,
    b: 0xec
  },
  'light blue': {
    r: 0xad,
    g: 0xdf,
    b: 0xff
  },
  'robin egg blue': {
    r: 0xbd,
    g: 0xed,
    b: 0xff
  },
  'pale blue lily': {
    r: 0xcf,
    g: 0xec,
    b: 0xec
  },
  'light cyan': {
    r: 0xe0,
    g: 0xff,
    b: 0xff
  },
  water: {
    r: 0xeb,
    g: 0xf4,
    b: 0xfa
  },
  aliceblue: {
    r: 0xf0,
    g: 0xf8,
    b: 0xff
  },
  azure: {
    r: 0xf0,
    g: 0xff,
    b: 0xff
  },
  'light slate': {
    r: 0xcc,
    g: 0xff,
    b: 0xff
  },
  'light aquamarine': {
    r: 0x93,
    g: 0xff,
    b: 0xe8
  },
  'electric blue': {
    r: 0x9a,
    g: 0xfe,
    b: 0xff
  },
  aquamarine: {
    r: 0x7f,
    g: 0xff,
    b: 0xd4
  },
  'cyan or aqua': {
    r: 0x00,
    g: 0xff,
    b: 0xff
  },
  'tron blue': {
    r: 0x7d,
    g: 0xfd,
    b: 0xfe
  },
  'blue zircon': {
    r: 0x57,
    g: 0xfe,
    b: 0xff
  },
  'blue lagoon': {
    r: 0x8e,
    g: 0xeb,
    b: 0xec
  },
  celeste: {
    r: 0x50,
    g: 0xeb,
    b: 0xec
  },
  'blue diamond': {
    r: 0x4e,
    g: 0xe2,
    b: 0xec
  },
  'tiffany blue': {
    r: 0x81,
    g: 0xd8,
    b: 0xd0
  },
  'cyan opaque': {
    r: 0x92,
    g: 0xc7,
    b: 0xc7
  },
  'blue hosta': {
    r: 0x77,
    g: 0xbf,
    b: 0xc7
  },
  'northern lights blue': {
    r: 0x78,
    g: 0xc7,
    b: 0xc7
  },
  'medium turquoise': {
    r: 0x48,
    g: 0xcc,
    b: 0xcd
  },
  turquoise: {
    r: 0x43,
    g: 0xc6,
    b: 0xdb
  },
  jellyfish: {
    r: 0x46,
    g: 0xc7,
    b: 0xc7
  },
  'blue green': {
    r: 0x7b,
    g: 0xcc,
    b: 0xb5
  },
  'macaw blue green': {
    r: 0x43,
    g: 0xbf,
    b: 0xc7
  },
  'light sea green': {
    r: 0x3e,
    g: 0xa9,
    b: 0x9f
  },
  'dark turquoise': {
    r: 0x3b,
    g: 0x9c,
    b: 0x9c
  },
  'sea turtle green': {
    r: 0x43,
    g: 0x8d,
    b: 0x80
  },
  'medium aquamarine': {
    r: 0x34,
    g: 0x87,
    b: 0x81
  },
  'greenish blue': {
    r: 0x30,
    g: 0x7d,
    b: 0x7e
  },
  'grayish turquoise': {
    r: 0x5e,
    g: 0x7d,
    b: 0x7e
  },
  'beetle green': {
    r: 0x4c,
    g: 0x78,
    b: 0x7e
  },
  teal: {
    r: 0x00,
    g: 0x80,
    b: 0x80
  },
  'sea green': {
    r: 0x4e,
    g: 0x89,
    b: 0x75
  },
  'camouflage green': {
    r: 0x78,
    g: 0x86,
    b: 0x6b
  },
  'sage green': {
    r: 0x84,
    g: 0x8b,
    b: 0x79
  },
  'hazel green': {
    r: 0x61,
    g: 0x7c,
    b: 0x58
  },
  'venom green': {
    r: 0x72,
    g: 0x8c,
    b: 0x00
  },
  'fern green': {
    r: 0x66,
    g: 0x7c,
    b: 0x26
  },
  'dark forest green': {
    r: 0x25,
    g: 0x41,
    b: 0x17
  },
  'medium sea green': {
    r: 0x30,
    g: 0x67,
    b: 0x54
  },
  'medium forest green': {
    r: 0x34,
    g: 0x72,
    b: 0x35
  },
  'seaweed green': {
    r: 0x43,
    g: 0x7c,
    b: 0x17
  },
  'pine green': {
    r: 0x38,
    g: 0x7c,
    b: 0x44
  },
  'jungle green': {
    r: 0x34,
    g: 0x7c,
    b: 0x2c
  },
  'shamrock green': {
    r: 0x34,
    g: 0x7c,
    b: 0x17
  },
  'medium spring green': {
    r: 0x34,
    g: 0x80,
    b: 0x17
  },
  'forest green': {
    r: 0x4e,
    g: 0x92,
    b: 0x58
  },
  'green onion': {
    r: 0x6a,
    g: 0xa1,
    b: 0x21
  },
  'spring green': {
    r: 0x4a,
    g: 0xa0,
    b: 0x2c
  },
  'lime green': {
    r: 0x41,
    g: 0xa3,
    b: 0x17
  },
  'clover green': {
    r: 0x3e,
    g: 0xa0,
    b: 0x55
  },
  'green snake': {
    r: 0x6c,
    g: 0xbb,
    b: 0x3c
  },
  'alien green': {
    r: 0x6c,
    g: 0xc4,
    b: 0x17
  },
  'green apple': {
    r: 0x4c,
    g: 0xc4,
    b: 0x17
  },
  'yellow green': {
    r: 0x52,
    g: 0xd0,
    b: 0x17
  },
  'kelly green': {
    r: 0x4c,
    g: 0xc5,
    b: 0x52
  },
  'zombie green': {
    r: 0x54,
    g: 0xc5,
    b: 0x71
  },
  'frog green': {
    r: 0x99,
    g: 0xc6,
    b: 0x8e
  },
  'green peas': {
    r: 0x89,
    g: 0xc3,
    b: 0x5c
  },
  'dollar bill green': {
    r: 0x85,
    g: 0xbb,
    b: 0x65
  },
  'dark sea green': {
    r: 0x8b,
    g: 0xb3,
    b: 0x81
  },
  'iguana green': {
    r: 0x9c,
    g: 0xb0,
    b: 0x71
  },
  'avocado green': {
    r: 0xb2,
    g: 0xc2,
    b: 0x48
  },
  'pistachio green': {
    r: 0x9d,
    g: 0xc2,
    b: 0x09
  },
  'salad green': {
    r: 0xa1,
    g: 0xc9,
    b: 0x35
  },
  'hummingbird green': {
    r: 0x7f,
    g: 0xe8,
    b: 0x17
  },
  'nebula green': {
    r: 0x59,
    g: 0xe8,
    b: 0x17
  },
  'stoplight go green': {
    r: 0x57,
    g: 0xe9,
    b: 0x64
  },
  'algae green': {
    r: 0x64,
    g: 0xe9,
    b: 0x86
  },
  'jade green': {
    r: 0x5e,
    g: 0xfb,
    b: 0x6e
  },
  green: {
    r: 0x00,
    g: 0xff,
    b: 0x00
  },
  'emerald green': {
    r: 0x5f,
    g: 0xfb,
    b: 0x17
  },
  'lawn green': {
    r: 0x87,
    g: 0xf7,
    b: 0x17
  },
  chartreuse: {
    r: 0x8a,
    g: 0xfb,
    b: 0x17
  },
  'dragon green': {
    r: 0x6a,
    g: 0xfb,
    b: 0x92
  },
  'mint green': {
    r: 0x98,
    g: 0xff,
    b: 0x98
  },
  'green thumb': {
    r: 0xb5,
    g: 0xea,
    b: 0xaa
  },
  'light jade': {
    r: 0xc3,
    g: 0xfd,
    b: 0xb8
  },
  'tea green': {
    r: 0xcc,
    g: 0xfb,
    b: 0x5d
  },
  'green yellow': {
    r: 0xb1,
    g: 0xfb,
    b: 0x17
  },
  'slime green': {
    r: 0xbc,
    g: 0xe9,
    b: 0x54
  },
  goldenrod: {
    r: 0xed,
    g: 0xda,
    b: 0x74
  },
  'harvest gold': {
    r: 0xed,
    g: 0xe2,
    b: 0x75
  },
  'sun yellow': {
    r: 0xff,
    g: 0xe8,
    b: 0x7c
  },
  yellow: {
    r: 0xff,
    g: 0xff,
    b: 0x00
  },
  'corn yellow': {
    r: 0xff,
    g: 0xf3,
    b: 0x80
  },
  parchment: {
    r: 0xff,
    g: 0xff,
    b: 0xc2
  },
  cream: {
    r: 0xff,
    g: 0xff,
    b: 0xcc
  },
  'lemon chiffon': {
    r: 0xff,
    g: 0xf8,
    b: 0xc6
  },
  cornsilk: {
    r: 0xff,
    g: 0xf8,
    b: 0xdc
  },
  beige: {
    r: 0xf5,
    g: 0xf5,
    b: 0xdc
  },
  blonde: {
    r: 0xfb,
    g: 0xf6,
    b: 0xd9
  },
  antiquewhite: {
    r: 0xfa,
    g: 0xeb,
    b: 0xd7
  },
  champagne: {
    r: 0xfb,
    g: 0xf6,
    b: 0xd9
  },
  blanchedalmond: {
    r: 0xff,
    g: 0xeb,
    b: 0xcd
  },
  vanilla: {
    r: 0xf3,
    g: 0xe5,
    b: 0xab
  },
  'tan brown': {
    r: 0xec,
    g: 0xe5,
    b: 0xb6
  },
  peach: {
    r: 0xff,
    g: 0xe5,
    b: 0xb4
  },
  mustard: {
    r: 0xff,
    g: 0xdb,
    b: 0x58
  },
  'rubber ducky yellow': {
    r: 0xff,
    g: 0xd8,
    b: 0x01
  },
  'bright gold': {
    r: 0xfd,
    g: 0xd0,
    b: 0x17
  },
  'golden brown': {
    r: 0xea,
    g: 0xc1,
    b: 0x17
  },
  'macaroni and cheese': {
    r: 0xf2,
    g: 0xbb,
    b: 0x66
  },
  saffron: {
    r: 0xfb,
    g: 0xb9,
    b: 0x17
  },
  beer: {
    r: 0xfb,
    g: 0xb1,
    b: 0x17
  },
  cantaloupe: {
    r: 0xff,
    g: 0xa6,
    b: 0x2f
  },
  'bee yellow': {
    r: 0xe9,
    g: 0xab,
    b: 0x17
  },
  'brown sugar': {
    r: 0xe2,
    g: 0xa7,
    b: 0x6f
  },
  burlywood: {
    r: 0xde,
    g: 0xb8,
    b: 0x87
  },
  'deep peach': {
    r: 0xff,
    g: 0xcb,
    b: 0xa4
  },
  'ginger brown': {
    r: 0xc9,
    g: 0xbe,
    b: 0x62
  },
  'school bus yellow': {
    r: 0xe8,
    g: 0xa3,
    b: 0x17
  },
  'sandy brown': {
    r: 0xee,
    g: 0x9a,
    b: 0x4d
  },
  'fall leaf brown': {
    r: 0xc8,
    g: 0xb5,
    b: 0x60
  },
  'orange gold': {
    r: 0xd4,
    g: 0xa0,
    b: 0x17
  },
  sand: {
    r: 0xc2,
    g: 0xb2,
    b: 0x80
  },
  'cookie brown': {
    r: 0xc7,
    g: 0xa3,
    b: 0x17
  },
  caramel: {
    r: 0xc6,
    g: 0x8e,
    b: 0x17
  },
  brass: {
    r: 0xb5,
    g: 0xa6,
    b: 0x42
  },
  khaki: {
    r: 0xad,
    g: 0xa9,
    b: 0x6e
  },
  'camel brown': {
    r: 0xc1,
    g: 0x9a,
    b: 0x6b
  },
  bronze: {
    r: 0xcd,
    g: 0x7f,
    b: 0x32
  },
  'tiger orange': {
    r: 0xc8,
    g: 0x81,
    b: 0x41
  },
  cinnamon: {
    r: 0xc5,
    g: 0x89,
    b: 0x17
  },
  'bullet shell': {
    r: 0xaf,
    g: 0x9b,
    b: 0x60
  },
  'dark goldenrod': {
    r: 0xaf,
    g: 0x78,
    b: 0x17
  },
  copper: {
    r: 0xb8,
    g: 0x73,
    b: 0x33
  },
  wood: {
    r: 0x96,
    g: 0x6f,
    b: 0x33
  },
  'oak brown': {
    r: 0x80,
    g: 0x65,
    b: 0x17
  },
  moccasin: {
    r: 0x82,
    g: 0x78,
    b: 0x39
  },
  'army brown': {
    r: 0x82,
    g: 0x7b,
    b: 0x60
  },
  sandstone: {
    r: 0x78,
    g: 0x6d,
    b: 0x5f
  },
  mocha: {
    r: 0x49,
    g: 0x3d,
    b: 0x26
  },
  taupe: {
    r: 0x48,
    g: 0x3c,
    b: 0x32
  },
  coffee: {
    r: 0x6f,
    g: 0x4e,
    b: 0x37
  },
  'brown bear': {
    r: 0x83,
    g: 0x5c,
    b: 0x3b
  },
  'red dirt': {
    r: 0x7f,
    g: 0x52,
    b: 0x17
  },
  sepia: {
    r: 0x7f,
    g: 0x46,
    b: 0x2c
  },
  'orange salmon': {
    r: 0xc4,
    g: 0x74,
    b: 0x51
  },
  rust: {
    r: 0xc3,
    g: 0x62,
    b: 0x41
  },
  'red fox': {
    r: 0xc3,
    g: 0x58,
    b: 0x17
  },
  chocolate: {
    r: 0xc8,
    g: 0x5a,
    b: 0x17
  },
  sedona: {
    r: 0xcc,
    g: 0x66,
    b: 0x00
  },
  'papaya orange': {
    r: 0xe5,
    g: 0x67,
    b: 0x17
  },
  'halloween orange': {
    r: 0xe6,
    g: 0x6c,
    b: 0x2c
  },
  'pumpkin orange': {
    r: 0xf8,
    g: 0x72,
    b: 0x17
  },
  'construction cone orange': {
    r: 0xf8,
    g: 0x74,
    b: 0x31
  },
  'sunrise orange': {
    r: 0xe6,
    g: 0x74,
    b: 0x51
  },
  'mango orange': {
    r: 0xff,
    g: 0x80,
    b: 0x40
  },
  'dark orange': {
    r: 0xf8,
    g: 0x80,
    b: 0x17
  },
  coral: {
    r: 0xff,
    g: 0x7f,
    b: 0x50
  },
  'basket ball orange': {
    r: 0xf8,
    g: 0x81,
    b: 0x58
  },
  'light salmon': {
    r: 0xf9,
    g: 0x96,
    b: 0x6b
  },
  tangerine: {
    r: 0xe7,
    g: 0x8a,
    b: 0x61
  },
  'dark salmon': {
    r: 0xe1,
    g: 0x8b,
    b: 0x6b
  },
  'light coral': {
    r: 0xe7,
    g: 0x74,
    b: 0x71
  },
  'bean red': {
    r: 0xf7,
    g: 0x5d,
    b: 0x59
  },
  'valentine red': {
    r: 0xe5,
    g: 0x54,
    b: 0x51
  },
  'shocking orange': {
    r: 0xe5,
    g: 0x5b,
    b: 0x3c
  },
  red: {
    r: 0xff,
    g: 0x00,
    b: 0x00
  },
  scarlet: {
    r: 0xff,
    g: 0x24,
    b: 0x00
  },
  'ruby red': {
    r: 0xf6,
    g: 0x22,
    b: 0x17
  },
  'ferrari red': {
    r: 0xf7,
    g: 0x0d,
    b: 0x1a
  },
  'fire engine red': {
    r: 0xf6,
    g: 0x28,
    b: 0x17
  },
  'lava red': {
    r: 0xe4,
    g: 0x22,
    b: 0x17
  },
  'love red': {
    r: 0xe4,
    g: 0x1b,
    b: 0x17
  },
  grapefruit: {
    r: 0xdc,
    g: 0x38,
    b: 0x1f
  },
  'chestnut red': {
    r: 0xc3,
    g: 0x4a,
    b: 0x2c
  },
  'cherry red': {
    r: 0xc2,
    g: 0x46,
    b: 0x41
  },
  mahogany: {
    r: 0xc0,
    g: 0x40,
    b: 0x00
  },
  'chilli pepper': {
    r: 0xc1,
    g: 0x1b,
    b: 0x17
  },
  cranberry: {
    r: 0x9f,
    g: 0x00,
    b: 0x0f
  },
  'red wine': {
    r: 0x99,
    g: 0x00,
    b: 0x12
  },
  burgundy: {
    r: 0x8c,
    g: 0x00,
    b: 0x1a
  },
  chestnut: {
    r: 0x95,
    g: 0x45,
    b: 0x35
  },
  'blood red': {
    r: 0x7e,
    g: 0x35,
    b: 0x17
  },
  sienna: {
    r: 0x8a,
    g: 0x41,
    b: 0x17
  },
  sangria: {
    r: 0x7e,
    g: 0x38,
    b: 0x17
  },
  firebrick: {
    r: 0x80,
    g: 0x05,
    b: 0x17
  },
  maroon: {
    r: 0x81,
    g: 0x05,
    b: 0x41
  },
  'plum pie': {
    r: 0x7d,
    g: 0x05,
    b: 0x41
  },
  'velvet maroon': {
    r: 0x7e,
    g: 0x35,
    b: 0x4d
  },
  'plum velvet': {
    r: 0x7d,
    g: 0x05,
    b: 0x52
  },
  'rosy finch': {
    r: 0x7f,
    g: 0x4e,
    b: 0x52
  },
  puce: {
    r: 0x7f,
    g: 0x5a,
    b: 0x58
  },
  'dull purple': {
    r: 0x7f,
    g: 0x52,
    b: 0x5d
  },
  'rosy brown': {
    r: 0xb3,
    g: 0x84,
    b: 0x81
  },
  'khaki rose': {
    r: 0xc5,
    g: 0x90,
    b: 0x8e
  },
  'pink bow': {
    r: 0xc4,
    g: 0x81,
    b: 0x89
  },
  'lipstick pink': {
    r: 0xc4,
    g: 0x87,
    b: 0x93
  },
  rose: {
    r: 0xe8,
    g: 0xad,
    b: 0xaa
  },
  'rose gold': {
    r: 0xec,
    g: 0xc5,
    b: 0xc0
  },
  'desert sand': {
    r: 0xed,
    g: 0xc9,
    b: 0xaf
  },
  'pig pink': {
    r: 0xfd,
    g: 0xd7,
    b: 0xe4
  },
  'cotton candy': {
    r: 0xfc,
    g: 0xdf,
    b: 0xff
  },
  'pink bubblegum': {
    r: 0xff,
    g: 0xdf,
    b: 0xdd
  },
  'misty rose': {
    r: 0xfb,
    g: 0xbb,
    b: 0xb9
  },
  pink: {
    r: 0xfa,
    g: 0xaf,
    b: 0xbe
  },
  'light pink': {
    r: 0xfa,
    g: 0xaf,
    b: 0xba
  },
  'flamingo pink': {
    r: 0xf9,
    g: 0xa7,
    b: 0xb0
  },
  'pink rose': {
    r: 0xe7,
    g: 0xa1,
    b: 0xb0
  },
  'pink daisy': {
    r: 0xe7,
    g: 0x99,
    b: 0xa3
  },
  'cadillac pink': {
    r: 0xe3,
    g: 0x8a,
    b: 0xae
  },
  'carnation pink': {
    r: 0xf7,
    g: 0x78,
    b: 0xa1
  },
  'blush red': {
    r: 0xe5,
    g: 0x6e,
    b: 0x94
  },
  'hot pink': {
    r: 0xf6,
    g: 0x60,
    b: 0xab
  },
  'watermelon pink': {
    r: 0xfc,
    g: 0x6c,
    b: 0x85
  },
  'violet red': {
    r: 0xf6,
    g: 0x35,
    b: 0x8a
  },
  'deep pink': {
    r: 0xf5,
    g: 0x28,
    b: 0x87
  },
  'pink cupcake': {
    r: 0xe4,
    g: 0x5e,
    b: 0x9d
  },
  'pink lemonade': {
    r: 0xe4,
    g: 0x28,
    b: 0x7c
  },
  'neon pink': {
    r: 0xf5,
    g: 0x35,
    b: 0xaa
  },
  magenta: {
    r: 0xff,
    g: 0x00,
    b: 0xff
  },
  ' dimorphotheca magenta': {
    r: 0xe3,
    g: 0x31,
    b: 0x9d
  },
  'bright neon pink': {
    r: 0xf4,
    g: 0x33,
    b: 0xff
  },
  'pale violet red': {
    r: 0xd1,
    g: 0x65,
    b: 0x87
  },
  'tulip pink': {
    r: 0xc2,
    g: 0x5a,
    b: 0x7c
  },
  'medium violet red': {
    r: 0xca,
    g: 0x22,
    b: 0x6b
  },
  'rogue pink': {
    r: 0xc1,
    g: 0x28,
    b: 0x69
  },
  'burnt pink': {
    r: 0xc1,
    g: 0x22,
    b: 0x67
  },
  'bashful pink': {
    r: 0xc2,
    g: 0x52,
    b: 0x83
  },
  'dark carnation pink': {
    r: 0xc1,
    g: 0x22,
    b: 0x83
  },
  plum: {
    r: 0xb9,
    g: 0x3b,
    b: 0x8f
  },
  'viola purple': {
    r: 0x7e,
    g: 0x58,
    b: 0x7e
  },
  'purple iris': {
    r: 0x57,
    g: 0x1b,
    b: 0x7e
  },
  'plum purple': {
    r: 0x58,
    g: 0x37,
    b: 0x59
  },
  indigo: {
    r: 0x4b,
    g: 0x00,
    b: 0x82
  },
  'purple monster': {
    r: 0x46,
    g: 0x1b,
    b: 0x7e
  },
  'purple haze': {
    r: 0x4e,
    g: 0x38,
    b: 0x7e
  },
  eggplant: {
    r: 0x61,
    g: 0x40,
    b: 0x51
  },
  grape: {
    r: 0x5e,
    g: 0x5a,
    b: 0x80
  },
  'purple jam': {
    r: 0x6a,
    g: 0x28,
    b: 0x7e
  },
  'dark orchid': {
    r: 0x7d,
    g: 0x1b,
    b: 0x7e
  },
  'purple flower': {
    r: 0xa7,
    g: 0x4a,
    b: 0xc7
  },
  'medium orchid': {
    r: 0xb0,
    g: 0x48,
    b: 0xb5
  },
  'purple amethyst': {
    r: 0x6c,
    g: 0x2d,
    b: 0xc7
  },
  'dark violet': {
    r: 0x84,
    g: 0x2d,
    b: 0xce
  },
  violet: {
    r: 0x8d,
    g: 0x38,
    b: 0xc9
  },
  'purple sage bush': {
    r: 0x7a,
    g: 0x5d,
    b: 0xc7
  },
  'lovely purple': {
    r: 0x7f,
    g: 0x38,
    b: 0xec
  },
  purple: {
    r: 0x8e,
    g: 0x35,
    b: 0xef
  },
  'aztech purple': {
    r: 0x89,
    g: 0x3b,
    b: 0xff
  },
  'medium purple': {
    r: 0x84,
    g: 0x67,
    b: 0xd7
  },
  'jasmine purple': {
    r: 0xa2,
    g: 0x3b,
    b: 0xec
  },
  'purple daffodil': {
    r: 0xb0,
    g: 0x41,
    b: 0xff
  },
  'tyrian purple': {
    r: 0xc4,
    g: 0x5a,
    b: 0xec
  },
  'crocus purple': {
    r: 0x91,
    g: 0x72,
    b: 0xec
  },
  'purple mimosa': {
    r: 0x9e,
    g: 0x7b,
    b: 0xff
  },
  'heliotrope purple': {
    r: 0xd4,
    g: 0x62,
    b: 0xff
  },
  crimson: {
    r: 0xe2,
    g: 0x38,
    b: 0xec
  },
  'purple dragon': {
    r: 0xc3,
    g: 0x8e,
    b: 0xc7
  },
  lilac: {
    r: 0xc8,
    g: 0xa2,
    b: 0xc8
  },
  'blush pink': {
    r: 0xe6,
    g: 0xa9,
    b: 0xec
  },
  mauve: {
    r: 0xe0,
    g: 0xb0,
    b: 0xff
  },
  'wisteria purple': {
    r: 0xc6,
    g: 0xae,
    b: 0xc7
  },
  'blossom pink': {
    r: 0xf9,
    g: 0xb7,
    b: 0xff
  },
  thistle: {
    r: 0xd2,
    g: 0xb9,
    b: 0xd3
  },
  periwinkle: {
    r: 0xe9,
    g: 0xcf,
    b: 0xec
  },
  'lavender pinocchio': {
    r: 0xeb,
    g: 0xdd,
    b: 0xe2
  },
  'lavender blue': {
    r: 0xe3,
    g: 0xe4,
    b: 0xfa
  },
  pearl: {
    r: 0xfd,
    g: 0xee,
    b: 0xf4
  },
  seashell: {
    r: 0xff,
    g: 0xf5,
    b: 0xee
  },
  'milk white': {
    r: 0xfe,
    g: 0xfc,
    b: 0xff
  },
  white: {
    r: 0xff,
    g: 0xff,
    b: 0xff
  }
}
