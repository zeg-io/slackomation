const colorMapper = require('../luxafor/color-map')

module.exports = color => Object.values(colorMapper[color])
