const { BooleanPrompt } = require('enquirer')

module.exports = async (key, message) => {
  const boolPrompt = new BooleanPrompt({
    name: key,
    message,
    default: true
  })
  return await boolPrompt.run()
}
