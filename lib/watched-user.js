const userProfileSimplifier = require('./user-profile-simplifier')
const actions = require('./actions')
let _watched = {}
let _registeredUserId

const actionTriggers = require('../actionTriggers')

const subscribeActions = () => {
  const actionMap = actionTriggers.filter(
    sub => sub[0] === _watched.status.emoji
  )[0]

  if (actionMap && actionMap[1]) {
    actions[actionMap[1]]()
    console.info(`💡 Color set to ${actionMap[1]}`)
  } else {
    actions.available()
  }
}

module.exports = {
  setUser: user => {
    _watched = userProfileSimplifier(user)

    subscribeActions()
  },
  watched: () => _watched,
  subscribeActions,
  registeredUserId: () => _registeredUserId,
  setRegisteredUser: user => {
    _registeredUserId = user.id
  }
}
