const usb = require('usb')

let deviceIsConnected = false
let vid
let pid

module.exports.init = ({
  deviceName,
  vendorId,
  productId,
  onConnect,
  onDisconnect
}) => {
  vid = vendorId
  pid = productId

  usb.on('attach', device => {
    const { idVendor, idProduct } = device.deviceDescriptor

    if (vid === idVendor && pid === idProduct) {
      deviceIsConnected = true
      console.info(`${deviceName} Connected`)
      onConnect()
    }
  })
  usb.on('detach', device => {
    const { idVendor, idProduct } = device.deviceDescriptor

    if (vid === idVendor && pid === idProduct) {
      deviceIsConnected = false
      console.info(`${deviceName} Disconnected`)
      onDisconnect()
    }
  })
}
module.exports.isConnected = () => !!usb.findByIds(vid, pid)
