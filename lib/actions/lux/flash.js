module.exports = (
  lux,
  seconds,
  { lightArray = [4, 5, 6], colorArray = ['red', 'black'], speed = 100 }
) =>
  new Promise((resolve, reject) => {
    let activeColor = 0
    let elapsedSec = 0

    const tick = setInterval(() => {
      activeColor += 1
      elapsedSec += 1
      if (activeColor >= colorArray.length) {
        activeColor = 0
      }
      lux
        .leds(lightArray)
        .fade(...colorName(colorArray[activeColor]), speed)
        .exec()

      if (elapsedSec >= seconds) {
        clearInterval(tick)
        resolve()
      }
    }, 2000)
  })
