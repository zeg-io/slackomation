module.exports = (
  lux,
  seconds,
  { lightArray = [4, 5, 6], colorArray = ['green', 'yellow', 'red'] }
) =>
  new Promise((resolve, reject) => {
    let activeLight = -1
    let activeColor = 0

    const steps = lightArray.length * colorArray.length
    const msPerStep = (seconds / steps) * 1000

    const tick = setInterval(() => {
      activeLight += 1
      if (activeLight >= lightArray.length) {
        activeLight = 0
        activeColor += 1
      }
      lux
        .led(lightArray[activeLight])
        .colorName(colorArray[activeColor])
        .exec()

      if (
        activeLight === lightArray.length - 1 &&
        activeColor === colorArray.length - 1
      ) {
        clearInterval(tick)
        resolve()
      }
    }, msPerStep)
  })
