const childProcess = require('child_process')
const Luxafor = require('../luxafor')
let lux
const flash = require('./lux/flash')
const { subscribeActions } = require('../watched-user')
let pid
let vid
module.exports.init = ({ vendorId, productId }) => {
  pid = productId
  vid = vendorId
  lux = new Luxafor(productId, vendorId)
}
const restart = () => {
  const childProc = childProcess.spawn(process.argv.shift(), process.argv, {
    cwd: process.cwd(),
    detached: true,
    stdio: 'inherit'
  })
  console.info(`Spawning new Luxafor Flag Process, PID: ${childProc.pid}
kill -9 ${childProc.pid} to terminate`)

  process.kill(process.pid, 'SIGINT')
}
// lux.color(red, green, blue)
module.exports.available = () => {
  try {
    lux.color(0, 255, 0).exec()
  } catch (e) {
    restart()
  }
}
module.exports.busy = () => {
  try {
    lux.color(255, 0, 0).exec()
  } catch (e) {
    restart()
  }
}
module.exports.headsDown = () => {
  try {
    lux.color(0, 0, 255).exec()
  } catch (e) {
    restart()
  }
}
module.exports.off = () => {
  try {
    lux.color(0, 0, 0).exec()
  } catch (e) {
    restart()
  }
}
module.exports.luxafor = () => {
  try {
    lux.pattern('luxafor').exec()
  } catch (e) {
    restart()
  }
}
module.exports.police = () => {
  try {
    lux.pattern('police').exec()
    setTimeout(() => {
      this.busy()
    }, 60000)
  } catch (e) {
    restart()
  }
}
module.exports.ringAndCall = () => {
  try {
    lux.pattern('police').exec()
    setTimeout(() => {
      this.busy()
    }, 3000)
  } catch (e) {
    restart()
  }
}
