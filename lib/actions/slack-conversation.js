module.exports = (messageEvent, wc) => {
  const { text, channel, user } = messageEvent
  const reply = text => {
    wc.chat.postMessage({
      channel,
      blocks: [
        // Result 1
        {
          type: 'section',
          text: {
            type: 'mrkdwn',
            text
          },
          accessory: {
            type: 'image',
            image_url:
              'https://avatars.slack-edge.com/2020-04-14/1063130413219_8f834d018066970b9bec_512.png',
            alt_text: 'Slackomator'
          }
        },
        {
          type: 'context',
          elements: [
            {
              type: 'plain_text',
              text: `Automate your Slack Statuses!`,
              emoji: true
            }
          ]
        },
        {
          type: 'divider'
        }
      ]
    })
    return text
  }

  let receivedMessage = text.trim().toLowerCase()

  switch (receivedMessage) {
    case 'help':
      return reply(
        `*Actions* are triggered when your status icon changes to an icon on your *Trigger* list. Here are the commands...
• \`action list\` List all the actions that are available
• \`trigger list\` List all the triggers that are set up
• \`trigger add <emojoi> <action>\` Add a new trigger
• \`trigger rm <emoji>\` remove a trigger`
      )
      break
    default:
      return reply('Not sure how to respond. try "help"')
  }
  return '...loud and clear!'
}
